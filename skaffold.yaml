apiVersion: skaffold/v1beta5
kind: Config
build:
  artifacts:
  - image: nlxio/docs
    context: docs
  - image: nlxio/ca-cfssl-unsafe
    context: ca-cfssl-unsafe
  - image: nlxio/ca-certportal
    context: .
    docker:
      dockerfile: ca-certportal/Dockerfile
  - image: nlxio/config-api
    context: .
    docker:
      dockerfile: config-api/Dockerfile
  - image: nlxio/directory-db
    context: .
    docker:
      dockerfile: directory-db/Dockerfile
  - image: nlxio/directory-inspection-api
    context: .
    docker:
      dockerfile: directory-inspection-api/Dockerfile
  - image: nlxio/directory-registration-api
    context: .
    docker:
      dockerfile: directory-registration-api/Dockerfile
  - image: nlxio/directory-monitor
    context: .
    docker:
      dockerfile: directory-monitor/Dockerfile
  - image: nlxio/directory-ui
    context: .
    docker:
      dockerfile: directory-ui/Dockerfile
  - image: nlxio/insight-ui
    context: .
    docker:
      dockerfile: insight-ui/Dockerfile
  - image: nlxio/inway
    context: .
    docker:
      dockerfile: inway/Dockerfile
  - image: nlxio/management-api
    context: .
    docker:
      dockerfile: management-api/Dockerfile
  - image: nlxio/management-ui
    context: .
    docker:
      dockerfile: management-ui/Dockerfile
  - image: nlxio/nlxctl
    context: .
    docker:
      dockerfile: nlxctl/Dockerfile
  - image: nlxio/outway
    context: .
    docker:
      dockerfile: outway/Dockerfile
  - image: nlxio/txlog-db
    context: .
    docker:
      dockerfile: txlog-db/Dockerfile
  - image: nlxio/insight-api
    context: .
    docker:
      dockerfile: insight-api/Dockerfile
  - image: nlxio/auth-service
    context: .
    docker:
      dockerfile: auth-service/Dockerfile
  tagPolicy:
    sha256: {}
  local:
    push: false
profiles:
- name: minikube
  deploy:
    helm:
      releases:
      - name: nlx-dev-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-dev.yaml
        values:
          caCertportalImage: nlxio/ca-certportal
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-dev-directory
      - name: nlx-dev-brp
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-dev-brp.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-dev-brp
      - name: nlx-dev-rdw
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-dev-rdw.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-dev-rdw
      - name: nlx-dev-haarlem
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-dev-haarlem.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          outwayImage: nlxio/outway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-dev-haarlem
- name: deploy-review-directory
  deploy:
    helm:
      releases:
      - name: "nlx-directory-{{.CI_ENVIRONMENT_SLUG}}"
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-review.yaml
        setValueTemplates:
          caCertportalImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/ca-certportal:{{.CI_COMMIT_SHORT_SHA}}"
          caCfsslUnsafeImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/ca-cfssl-unsafe:{{.CI_COMMIT_SHORT_SHA}}"
          directoryDBImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/directory-db:{{.CI_COMMIT_SHORT_SHA}}"
          directoryInspectionImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/directory-inspection-api:{{.CI_COMMIT_SHORT_SHA}}"
          directoryMonitorImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/directory-monitor:{{.CI_COMMIT_SHORT_SHA}}"
          directoryRegistrationImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/directory-registration-api:{{.CI_COMMIT_SHORT_SHA}}"
          directoryUIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/directory-ui:{{.CI_COMMIT_SHORT_SHA}}"
          docsImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/docs:{{.CI_COMMIT_SHORT_SHA}}"
          insightUIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/insight-ui:{{.CI_COMMIT_SHORT_SHA}}"
          caAddress: ca-cfssl-unsafe.nlx-directory-$CI_ENVIRONMENT_SLUG
          disableLoadBalancers: true
          insightDirectoryInspectionAPIURL: http://directory-inspection-api-internal.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          externalDomain: nlx-directory-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          domain: nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
- name: deploy-review-brp
  deploy:
    helm:
      releases:
      - name: "nlx-brp-{{.CI_ENVIRONMENT_SLUG}}"
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-review-brp.yaml
        setValueTemplates:
          authServiceImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/auth-service:{{.CI_COMMIT_SHORT_SHA}}"
          caCfsslUnsafeImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/ca-cfssl-unsafe:{{.CI_COMMIT_SHORT_SHA}}"
          configAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/config-api:{{.CI_COMMIT_SHORT_SHA}}"
          insightAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/insight-api:{{.CI_COMMIT_SHORT_SHA}}"
          inwayImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/inway:{{.CI_COMMIT_SHORT_SHA}}"
          managementAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-api:{{.CI_COMMIT_SHORT_SHA}}"
          managementUIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-ui:{{.CI_COMMIT_SHORT_SHA}}"
          nlxctlImage:  "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/nlxctl:{{.CI_COMMIT_SHORT_SHA}}"
          txlogDBImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/txlog-db:{{.CI_COMMIT_SHORT_SHA}}"
          directoryRegistrationHostname: directory-registration-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          directoryInspectionHostname: directory-inspection-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          caAddress: ca-cfssl-unsafe.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          externalDomain: nlx-brp-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          domain: nlx-brp-{{.CI_ENVIRONMENT_SLUG}}
          inway.config: |
            [services]
                [services.basisregistratie]
                endpoint-url = 'https://service:8443'
                authorization-model = 'none'
                api-specification-document-url = 'https://service:8443/openapi.json'
                insight-api-url = 'https://insight-api-nlx-brp-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}'
                irma-api-url = 'https://irma-api-nlx-brp-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}'
                ca-cert-path = '/ca-certs/basisregistratie.pem'
- name: deploy-review-rdw
  deploy:
    helm:
      releases:
      - name: "nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}"
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-review-rdw.yaml
        setValueTemplates:
          authServiceImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/auth-service:{{.CI_COMMIT_SHORT_SHA}}"
          caCfsslUnsafeImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/ca-cfssl-unsafe:{{.CI_COMMIT_SHORT_SHA}}"
          configAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/config-api:{{.CI_COMMIT_SHORT_SHA}}"
          insightAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/insight-api:{{.CI_COMMIT_SHORT_SHA}}"
          inwayImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/inway:{{.CI_COMMIT_SHORT_SHA}}"
          managementAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-api:{{.CI_COMMIT_SHORT_SHA}}"
          managementUIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-ui:{{.CI_COMMIT_SHORT_SHA}}"
          nlxctlImage:  "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/nlxctl:{{.CI_COMMIT_SHORT_SHA}}"
          txlogDBImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/txlog-db:{{.CI_COMMIT_SHORT_SHA}}"
          directoryRegistrationHostname: directory-registration-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          directoryInspectionHostname: directory-inspection-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          caAddress: ca-cfssl-unsafe.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          externalDomain: nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          domain: nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}
          configAPI.insightAPIURL: https://insight-api-nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          configAPI.irmaServerURL: https://irma-api-nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          configAPI.serviceConfig: |
            \{
              "name": "kentekenregister"\,
              "endpointURL": "http://service.nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}:8000"\,
              "apiSpecificationURL": "http://service.nlx-rdw-{{.CI_ENVIRONMENT_SLUG}}:8000/openapi.yaml"\,
              "authorizationSettings": \{
                "mode": "none"
              \}\,
              "inways": ["inway.rdw"]
            \}

- name: deploy-review-haarlem
  deploy:
    helm:
      releases:
      - name: "nlx-haarlem-{{.CI_ENVIRONMENT_SLUG}}"
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-review-haarlem.yaml
        setValueTemplates:
          authServiceImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/auth-service:{{.CI_COMMIT_SHORT_SHA}}"
          caCfsslUnsafeImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/ca-cfssl-unsafe:{{.CI_COMMIT_SHORT_SHA}}"
          configAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/config-api:{{.CI_COMMIT_SHORT_SHA}}"
          insightAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/insight-api:{{.CI_COMMIT_SHORT_SHA}}"
          inwayImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/inway:{{.CI_COMMIT_SHORT_SHA}}"
          managementAPIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-api:{{.CI_COMMIT_SHORT_SHA}}"
          managementUIImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/management-ui:{{.CI_COMMIT_SHORT_SHA}}"
          nlxctlImage:  "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/nlxctl:{{.CI_COMMIT_SHORT_SHA}}"
          outwayImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/outway:{{.CI_COMMIT_SHORT_SHA}}"
          txlogDBImage: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/nlxio/txlog-db:{{.CI_COMMIT_SHORT_SHA}}"
          directoryRegistrationHostname: directory-registration-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          directoryInspectionHostname: directory-inspection-api.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          caAddress: ca-cfssl-unsafe.nlx-directory-{{.CI_ENVIRONMENT_SLUG}}
          externalDomain: nlx-haarlem-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}
          domain: nlx-haarlem-{{.CI_ENVIRONMENT_SLUG}}
          inway.config: |
            [services]
                [services.demo-api]
                endpoint-url = 'https://postman-echo.com'
                authorization-model = 'none'
                insight-api-url = 'https://insight-api-nlx-haarlem-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}'
                irma-api-url = 'https://irma-api-nlx-haarlem-{{.CI_ENVIRONMENT_SLUG}}.{{.REVIEW_BASE_DOMAIN}}'
                public-support-contact = "public-support@nlx.io"
                tech-support-contact = "tech-support@nlx.io"

- name: release-review
  build:
    tagPolicy:
       envTemplate:
         template: "{{.CI_REGISTRY}}/{{.CI_PROJECT_PATH}}/{{.IMAGE_NAME}}:{{.CI_COMMIT_SHORT_SHA}}"
    local:
      push: true
- name: release
  build:
    tagPolicy:
      gitCommit: {}
    local:
      push: true
- name: deploy-test
  deploy:
    helm:
      releases:
      - name: nlx-test-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-test.yaml
        values:
          caCertportalImage: nlxio/ca-certportal
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-test-directory
      - name: nlx-test-brp
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-test-brp.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-test-brp
      - name: nlx-test-rdw
        chartPath: helm/nlx-organization
        setValueTemplates:
         management.basicAuth: "{{.MANAGEMENT_BASIC_AUTH}}"
        valuesFiles:
        - helm/nlx-organization/values-test-rdw.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-test-rdw
      - name: nlx-test-haarlem
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-test-haarlem.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          outwayImage: nlxio/outway
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-test-haarlem
- name: deploy-acc
  deploy:
    helm:
      releases:
      - name: nlx-acc-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-acc.yaml
        values:
          caCertportalImage: nlxio/ca-certportal
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-acc-directory
      - name: nlx-acc-brp
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-acc-brp.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-acc-brp
      - name: nlx-acc-rdw
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-acc-rdw.yaml
        setValueTemplates:
         management.basicAuth: "{{.MANAGEMENT_BASIC_AUTH}}"
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-acc-rdw
      - name: nlx-acc-haarlem
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-acc-haarlem.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          outwayImage: nlxio/outway
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-acc-haarlem
- name: deploy-demo
  deploy:
    helm:
      releases:
      - name: nlx-demo-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-demo.yaml
        values:
          caCertportalImage: nlxio/ca-certportal
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-demo-directory
      - name: nlx-demo-brp
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-demo-brp.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-demo-brp
      - name: nlx-demo-rdw
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-demo-rdw.yaml
        setValueTemplates:
         management.basicAuth: "{{.MANAGEMENT_BASIC_AUTH}}"
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-demo-rdw
      - name: nlx-demo-haarlem
        chartPath: helm/nlx-organization
        valuesFiles:
        - helm/nlx-organization/values-demo-haarlem.yaml
        values:
          authServiceImage: nlxio/auth-service
          caCfsslUnsafeImage: nlxio/ca-cfssl-unsafe
          configAPIImage: nlxio/config-api
          insightAPIImage: nlxio/insight-api
          inwayImage: nlxio/inway
          outwayImage: nlxio/outway
          managementAPIImage: nlxio/management-api
          managementUIImage: nlxio/management-ui
          nlxctlImage: nlxio/nlxctl
          txlogDBImage: nlxio/txlog-db
        namespace: nlx-demo-haarlem
- name: deploy-preprod
  deploy:
    helm:
      releases:
      - name: nlx-preprod-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-preprod.yaml
        values:
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-preprod-directory
- name: deploy-prod
  deploy:
    helm:
      releases:
      - name: nlx-prod-directory
        chartPath: helm/nlx-directory
        valuesFiles:
        - helm/nlx-directory/values-prod.yaml
        values:
          directoryDBImage: nlxio/directory-db
          directoryInspectionImage: nlxio/directory-inspection-api
          directoryMonitorImage: nlxio/directory-monitor
          directoryRegistrationImage: nlxio/directory-registration-api
          directoryUIImage: nlxio/directory-ui
          docsImage: nlxio/docs
          insightUIImage: nlxio/insight-ui
        namespace: nlx-prod-directory
