ALTER TABLE directory.services
	ADD COLUMN tech_support_contact character varying(250),
	ADD COLUMN public_support_contact character varying(250);
