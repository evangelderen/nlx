// Copyright © VNG Realisatie 2019
// Licensed under the EUPL

import styled from 'styled-components'

export const StyledApp = styled.div`
    padding-top: 56px;
`
