// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

import styled from 'styled-components'
import { Pagination } from '@commonground/design-system'

export const StyledLogsPage = styled.div`
  padding-bottom: 40px;
  padding-top: 40px;
  text-align: center;
`

export const StyledPagination = styled(Pagination)`
  display: inline-block;
  margin-top: 20px;
`

