// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

import styled from 'styled-components'
import {Card} from '@commonground/design-system'

export const StyledCard = styled(Card)`
  width: 400px;
  padding: 8px 24px 8px 24px;
  
  h1 {
    color: #FE3A24;
    font-size: 20px;
    font-weight: 600;
  }
`
