// Copyright © VNG Realisatie 2019
// Licensed under the EUPL
import React from 'react'
import PageTemplate from '../../components/PageTemplate'

const Home = () => (
    <PageTemplate>Welcome to the NLX management interface.</PageTemplate>
)

export default Home
