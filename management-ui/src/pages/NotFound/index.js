// Copyright © VNG Realisatie 2019
// Licensed under the EUPL
import React from 'react'
import PageTemplate from '../../components/PageTemplate'

const NotFound = () => <PageTemplate>This page does not exist.</PageTemplate>

export default NotFound
