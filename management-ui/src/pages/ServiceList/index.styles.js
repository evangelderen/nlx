// Copyright © VNG Realisatie 2019
// Licensed under the EUPL
import styled from 'styled-components'

export const StyledTopButtons = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-bottom: 1em;
`
