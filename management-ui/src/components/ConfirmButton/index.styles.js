// Copyright © VNG Realisatie 2019
// Licensed under the EUPL
import styled from 'styled-components'

export const StyledConfirm = styled.div`
    display: flex;

    div {
        height: 40px;
        line-height: 40px;
    }

    div,
    button {
        margin-right: 10px;
    }
`
