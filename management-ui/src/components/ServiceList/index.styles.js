// Copyright © VNG Realisatie 2019
// Licensed under the EUPL
import styled from 'styled-components'
import { Card as DesignSystemCard } from '@commonground/design-system'

export const Card = styled(DesignSystemCard)`
    padding: 10px;

    a {
        color: #000;
        text-decoration: none;
    }
`
