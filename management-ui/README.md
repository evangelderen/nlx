Management UI
============

The *Management UI* provides a human-friendly interface to manage the NLX components within an organization.
Within this interface the administrator can:

* view all active NLX components
* create/update/delete services
