---
title: "Get started"
description: ""
---

Welcome! We are excited that you want to learn about NLX.

The NLX Get Started Tutorial teaches you how to:

1. [Setup your environment](./setup-your-environment/)
1. [Create certificates](./create-certificates/)
1. [Consume an API on the NLX network](./consume-an-api/)
1. [Provide an API to the NLX network](./provide-an-api/)

This guide will use the NLX demo services. Once you're ready to consume or provide your services
to the public, contact us so we can setup access to the production environment.
