// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

hljs.initHighlightingOnLoad();
mermaid.initialize({
    startOnLoad:true
});
